package com.example.project1;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

public class SearchSetting extends Fragment implements NumberPicker.OnValueChangeListener, OnClickListener{
	TextView txt_of_age_between;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.search_setting, container, false);
		
		txt_of_age_between = (TextView) v.findViewById(R.id.of_age_between);
		
		txt_of_age_between.setOnClickListener(this);
		
		return v;
	}

	@Override
	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
		Log.i("value is", "" + newVal);
	}
	
	public void showDialog(){
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.dialog_of_searchsetting);
		dialog.setTitle("of age between");
		
		Button bnt_set = (Button) dialog.findViewById(R.id.btn_set);
		Button bnt_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
		
		final NumberPicker np1 = (NumberPicker) dialog.findViewById(R.id.numberPicker1);
		final NumberPicker np2 = (NumberPicker) dialog.findViewById(R.id.numberPicker2);
		
		np1.setMaxValue(140);
		np1.setMinValue(18);
		np1.setWrapSelectorWheel(false);
		np1.setOnValueChangedListener(this);
		
		np2.setMaxValue(140);
		np2.setMinValue(18);
		np2.setWrapSelectorWheel(false);
		np2.setOnValueChangedListener(this);
		
		bnt_set.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if( np1.getValue() > np2.getValue() ){
					Toast.makeText(getActivity(), "False" , Toast.LENGTH_SHORT).show();
				}else{
					txt_of_age_between.setText( String.valueOf(np1.getValue()) + " and " + String.valueOf(np2.getValue()) );
					Toast.makeText(getActivity(), String.valueOf(np1.getValue()) + " and " + String.valueOf(np2.getValue()) , Toast.LENGTH_SHORT).show();
				}
				
				dialog.dismiss();
			}
		});
		
		bnt_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.of_age_between:
			showDialog();
			break;

		default:
			break;
		}
		
	}
	
	
	
	
}






