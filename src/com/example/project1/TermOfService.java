package com.example.project1;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class TermOfService extends Fragment{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.term_of_service, container, false);
		
		WebView myWebView = (WebView) v.findViewById(R.id.webView1);
		myWebView.loadUrl("http://google.com.vn/");
		WebSettings webSettings = myWebView.getSettings();
		webSettings.setBuiltInZoomControls(true);
		
		return v;
	}
}