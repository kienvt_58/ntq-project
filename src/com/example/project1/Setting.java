package com.example.project1;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class Setting extends Fragment implements OnClickListener {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.setting, container, false);
		
		v.findViewById(R.id.imageView1).setOnClickListener(this);
		v.findViewById(R.id.imageView2).setOnClickListener(this);
		v.findViewById(R.id.imageView3).setOnClickListener(this);
		v.findViewById(R.id.imageView7).setOnClickListener(this);

		
		return v;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imageView1:
			getFragmentManager().beginTransaction().replace(R.id.frame, new Notifications() ).addToBackStack(null).commit();
			break;
		case R.id.imageView2:
			getFragmentManager().beginTransaction().replace(R.id.frame, new DistanceIn() ).addToBackStack(null).commit();
			break;
		case R.id.imageView3:
			getFragmentManager().beginTransaction().replace(R.id.frame, new ChangePassword() ).addToBackStack(null).commit();
			break;
		case R.id.imageView7:
			getFragmentManager().beginTransaction().replace(R.id.frame, new TermOfService() ).addToBackStack(null).commit();
			break;
			
		default:
			break;
		}
		
	}
	
}
