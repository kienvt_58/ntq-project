package com.example.project1;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class Menu extends Fragment implements OnClickListener{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.menu,container, false);
		
		v.findViewById(R.id.textView11).setOnClickListener(this);
		v.findViewById(R.id.textView12).setOnClickListener(this);
		v.findViewById(R.id.textView1).setOnClickListener(this);
		
		return v;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.textView11:
			getFragmentManager().beginTransaction().replace(R.id.frame, new Point()).addToBackStack(null).commit();
			break;
		case R.id.textView12:
			getFragmentManager().beginTransaction().replace(R.id.frame, new Setting()).addToBackStack(null).commit();
			break;
		case R.id.textView1:
			getFragmentManager().beginTransaction().replace(R.id.frame, new SearchSetting()).addToBackStack(null).commit();
			break;
		default:
			break;
		}
		
	}
	
	
	
	
}
